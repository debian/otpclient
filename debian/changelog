otpclient (4.0.2-1) unstable; urgency=medium

  * New upstream version 4.0.2.
  * Bump Standards-Version to 4.7.0.

 -- Francisco Vilmar Cardoso Ruviaro <vilmar@debian.org>  Fri, 16 Aug 2024 23:09:33 +0000

otpclient (4.0.1-1) unstable; urgency=medium

  * New upstream version 4.0.1.
  * Update debian/otpclient.install to add the missing ui file.

 -- Francisco Vilmar Cardoso Ruviaro <vilmar@debian.org>  Fri, 02 Aug 2024 11:34:43 +0000

otpclient (4.0.0-1) unstable; urgency=medium

  * New upstream version 4.0.0.

 -- Francisco Vilmar Cardoso Ruviaro <vilmar@debian.org>  Tue, 30 Jul 2024 15:53:34 +0000

otpclient (3.7.0-1) unstable; urgency=medium

  * New upstream version 3.7.0.
  * Drop debian/manpage/.
  * Drop debian/not-installed.
  * Drop debian/otpclient-cli.1.
  * Drop debian/otpclient.1.
  * debian/otpclient-cli.manpages file pointing to upstream.
  * debian/otpclient.manpages file pointing to upstream.
  * Drop debian/patches/002-fix-freeotp-export.patch.
  * Update debian/otpclient-cli.install.
  * Update debian/otpclient.install.

 -- Francisco Vilmar Cardoso Ruviaro <vilmar@debian.org>  Wed, 24 Jul 2024 15:59:30 +0000

otpclient (3.6.0-3) unstable; urgency=medium

  * Add debian/patches/002-fix-freeotp-export.patch. (Closes: #1074263)

 -- Francisco Vilmar Cardoso Ruviaro <vilmar@debian.org>  Thu, 27 Jun 2024 14:01:23 +0000

otpclient (3.6.0-2) unstable; urgency=medium

  * Update debian/otpclient-cli.1. (Closes: #1074260)

 -- Francisco Vilmar Cardoso Ruviaro <vilmar@debian.org>  Wed, 26 Jun 2024 15:51:36 +0000

otpclient (3.6.0-1) unstable; urgency=medium

  * New upstream version 3.6.0.
  * Refresh debian/patches/001-use_debhelper_CFLAGS.patch.
  * Update debian/otpclient.install.
  * Change from pkg-config to pkgconf in the Build-Depends field.
  * Update upstream and packaging copyright years.

 -- Francisco Vilmar Cardoso Ruviaro <vilmar@debian.org>  Mon, 29 Apr 2024 00:51:43 +0000

otpclient (3.2.1-2) unstable; urgency=medium

  * Rebuild against libcotp (2.1.0-1). (Closes: #1057369)

 -- Francisco Vilmar Cardoso Ruviaro <vilmar@debian.org>  Mon, 11 Dec 2023 17:05:45 +0000

otpclient (3.2.1-1) unstable; urgency=medium

  * New upstream version 3.2.1.

 -- Francisco Vilmar Cardoso Ruviaro <vilmar@debian.org>  Mon, 06 Nov 2023 17:15:36 +0000

otpclient (3.1.9-1) unstable; urgency=medium

  * New upstream version 3.1.9.
  * Update upstream copyright years.

 -- Francisco Vilmar Cardoso Ruviaro <vilmar@debian.org>  Tue, 25 Jul 2023 12:51:06 +0000

otpclient (3.1.8-2) unstable; urgency=medium

  * Remove libbaseencode-dev from Build-Depends field.

 -- Francisco Vilmar Cardoso Ruviaro <vilmar@debian.org>  Tue, 27 Jun 2023 18:35:45 +0000

otpclient (3.1.8-1) unstable; urgency=medium

  * New upstream version 3.1.8.

 -- Francisco Vilmar Cardoso Ruviaro <vilmar@debian.org>  Tue, 27 Jun 2023 18:25:16 +0000

otpclient (3.1.4-1) unstable; urgency=medium

  * New upstream version 3.1.4.

 -- Francisco Vilmar Cardoso Ruviaro <vilmar@debian.org>  Mon, 27 Feb 2023 11:58:30 +0000

otpclient (3.1.3-1) unstable; urgency=medium

  * New upstream version 3.1.3.

 -- Francisco Vilmar Cardoso Ruviaro <vilmar@debian.org>  Tue, 17 Jan 2023 11:50:22 +0000

otpclient (3.1.1-1) unstable; urgency=medium

  * New upstream version 3.1.1.
  * Refresh debian/patches/001-use_debhelper_CFLAGS.patch.
  * Add new build-depends 'libqrencode-dev'.

 -- Francisco Vilmar Cardoso Ruviaro <vilmar@debian.org>  Thu, 05 Jan 2023 16:18:02 +0000

otpclient (2.6.3-2) unstable; urgency=medium

  * Update the search rule in debian/watch.
  * Drop debian/salsa-ci.yml.
  * Update packaging copyright years.
  * Bump Standards-Version to 4.6.2.

 -- Francisco Vilmar Cardoso Ruviaro <vilmar@debian.org>  Tue, 03 Jan 2023 14:40:51 +0000

otpclient (2.6.3-1) unstable; urgency=medium

  * New upstream version 2.6.3.

 -- Francisco Vilmar Cardoso Ruviaro <vilmar@debian.org>  Mon, 08 Aug 2022 13:37:14 +0000

otpclient (2.6.2-1) unstable; urgency=medium

  * New upstream version 2.6.2.
  * Bump Standards-Version to 4.6.1.
  * Add new build dependencies: 'libbaseencode-dev', 'libprotobuf-c-dev',
    'libprotobuf-dev' and 'libsecret-1-dev'.
  * Refresh all patches.

 -- Francisco Vilmar Cardoso Ruviaro <vilmar@debian.org>  Thu, 14 Jul 2022 15:32:09 +0000

otpclient (2.5.1-1) unstable; urgency=medium

  * New upstream version 2.5.1.

 -- Francisco Vilmar Cardoso Ruviaro <vilmar@debian.org>  Mon, 11 Apr 2022 12:12:48 +0000

otpclient (2.5.0-1) unstable; urgency=medium

  * New upstream version 2.5.0.

 -- Francisco Vilmar Cardoso Ruviaro <vilmar@debian.org>  Thu, 31 Mar 2022 17:08:20 +0000

otpclient (2.4.9.1-1) unstable; urgency=medium

  * New upstream version 2.4.9.1.
  * Update my email address.
  * Update upstream and packaging copyright years.

 -- Francisco Vilmar Cardoso Ruviaro <vilmar@debian.org>  Mon, 07 Mar 2022 01:18:23 +0000

otpclient (2.4.6-1) unstable; urgency=medium

  * New upstream version 2.4.6.
  * debian/copyright: update upstream and packaging copyright years.

 -- Francisco Vilmar Cardoso Ruviaro <vilmar@debian.org>  Tue, 16 Nov 2021 13:41:25 +0000

otpclient (2.4.5-1) unstable; urgency=medium

  * New upstream version 2.4.5.
  * Bump Standards-Version to 4.6.0.1.
  * debian/tests/control: drop the superficial test and drop Test-Command that
    runs in the background with an ampersand. See #988591.

 -- Francisco Vilmar Cardoso Ruviaro <vilmar@debian.org>  Sat, 13 Nov 2021 00:55:05 +0000

otpclient (2.4.4-1) unstable; urgency=medium

  * New upstream version 2.4.4.
  * Update debian/watch.
  * debian/patches/001-use_debhelper_CFLAGS.patch: refresh.

 -- Francisco Vilmar Cardoso Ruviaro <vilmar@debian.org>  Tue, 17 Aug 2021 01:32:41 +0000

otpclient (2.4.2-1) unstable; urgency=medium

  * New upstream version 2.4.2.
  * Update debian/otpclient.install.

 -- Francisco Vilmar Cardoso Ruviaro <vilmar@debian.org>  Fri, 12 Feb 2021 19:34:39 +0000

otpclient (2.4.1-1) unstable; urgency=medium

  * New upstream version 2.4.1.
  * debian/manpage/otpclient-cli.txt: update.
  * debian/otpclient-cli.1: update.
  * debian/tests/control: add to perform a test.

 -- Francisco Vilmar Cardoso Ruviaro <vilmar@debian.org>  Wed, 06 Jan 2021 18:25:54 +0000

otpclient (2.4.0-1) unstable; urgency=medium

  * New upstream version 2.4.0.
  * Bump Standards-Version to 4.5.1.
  * debian/gbp.conf: replace debian/master with debian/latest.

 -- Francisco Vilmar Cardoso Ruviaro <vilmar@debian.org>  Thu, 31 Dec 2020 05:07:48 +0000

otpclient (2.3.2-1) unstable; urgency=medium

  * New upstream version 2.3.2.
  * debian/not-installed: created to list the files otpclient.1.gz and
    otpclient-cli.1.gz. Using the packaging manpage.
  * debian/otpclient.1: updated date and version.
  * debian/otpclient.install: added new icon.
  * debian/otpclient-cli.1: updated date and version.
  * debian/patches/001-use_debhelper_CFLAGS.patch:
      - Added 'Forwarded: not-needed'.
  * debian/watch: fixed syntax of pgpsigurlmangle option.

 -- Francisco Vilmar Cardoso Ruviaro <vilmar@debian.org>  Sat, 01 Aug 2020 21:54:18 +0000

otpclient (2.3.1-2) unstable; urgency=medium

  * Source-only upload to allow testing migration.

 -- Francisco Vilmar Cardoso Ruviaro <vilmar@debian.org>  Wed, 15 Jul 2020 16:11:32 +0000

otpclient (2.3.1-1) unstable; urgency=low

  * Initial release. (Closes: #959692)

 -- Francisco Vilmar Cardoso Ruviaro <vilmar@debian.org>  Sat, 20 Jun 2020 21:49:23 +0000
